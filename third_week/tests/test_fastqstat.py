import unittest
from third_week.fastqstat import get_reads_avg_length_and_count


class TestFastqstat(unittest.TestCase):
    def test_empty_file(self):
        with open('third_week/tests/empty.fastq', 'r') as in_f:
            self.assertEqual(get_reads_avg_length_and_count(in_f), (0, 0))

    def test_regular_file(self):
        with open('third_week/tests/regular.fastq', 'r') as in_f:
            self.assertEqual(get_reads_avg_length_and_count(in_f), (7.5, 4))


if __name__ == '__main__':
    unittest.main()
