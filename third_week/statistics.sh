#!/bin/bash

WORK_DIR=/home/metaclass/AleksandrSl/metagenomics/third_week/data/
JOINED_READS_DIR=$WORK_DIR/joined_reads
RES_PATH=$JOINED_READS_DIR/statistics.tsv
FASTQSTAT=/home/metaclass/AleksandrSl/metagenomics/third_week/fastqstat.py

# Create file with statistics and add header
echo -e 'filename\tnseqs\tseqlen' > $RES_PATH

for file in ${JOINED_READS_DIR}/*.fastq
do 
    stats="$($FASTQSTAT < $file)"
    echo -e $file'\t'$stats >> $RES_PATH
    echo $file is analysed
done
