#!/usr/bin/env python3
from functools import reduce
import random
from sys import stdin
from typing import TextIO

# from Bio import SeqIO


def sum_and_count(x: (int, int), y: (int, int)) -> (float, int):
    return x[0] + y[0], x[1] + y[1]

def avg_and_count(x: int, y: int) -> float:
    # print((x[0] + y[0]) / 2, x[1] + y[1])
    return (x + y) / 2


def get_reads_avg_length_and_count(in_f: TextIO) -> (float, int):
    total_length, count = reduce(sum_and_count, [(len(read.seq), 1) for read in SeqIO.parse(in_f, "fastq")], [0, 0])
    return (total_length / count, count) if count else (0, 0)


def test(l):
    return l[0] / (2** (len(l) - 1)) + sum((l[i] / (2 ** (len(l) - i))) for i in range(1, len(l)))



def gen_random_list():
    return [random.randint(0, 1000) for _ in range(100)]


for _ in range(10000):
    random_list = gen_random_list()
    assert reduce(avg_and_count, random_list) == test(random_list)
# l = [0, 2, 4, 6, 8]
# for i in range(1, len(l) - 1):
#     print(len(l) - i)
#     print('power: {}'.format(2 ** (len(l) - i)))
#     print('WTF: {}'.format(2 ** 4))
#     print((l[i] / (2 ** (len(l) - i))))


# if __name__ == '__main__':
#     avg_len, count = get_reads_avg_length_and_count(stdin)
#     print(f'{count}\t{avg_len}')
